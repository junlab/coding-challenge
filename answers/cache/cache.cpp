/*
 * Implementation of the class that stores the cache objects (made on ubuntu)
 * Author: Valdir Dias Silva Junior
 * Email: valdir.d.siva.junior@gmail.com
 */

#include <iostream>
#include <ctime>
#include <string>
#include "cache.h"

using namespace std;

template <class T>
Cache<T>::Cache(int s) {
	size = s;
	complete = 0;
	front = 0;
	circleQ = new RegCache[size];
}

template <class T>
Cache<T>::~Cache(){}

template <class T>
void Cache<T>::add(T value) {
	circleQ[front].cache = value;
	circleQ[front].time = time(0);
	if (front < size - 1) {
		front++;
	} else {
		complete = 1;
		front = 0;
	}
	cout <<"ok!"<<endl;
}

template <class T>
void Cache<T>::print(bool ln){
	string barraN = "";
	if (ln) {
		barraN = "\n";
	}
	if (complete) {
		for (int i = 0; i < size; i++)
	        cout<<circleQ[i].cache<<" "<<barraN;
	} else {
		for (int i = 0; i < front; i++)
	        cout<<circleQ[i].cache<<" "<<barraN;
	}
	cout<<endl;
}

int main(int argc, char **argv) {
	Cache<int> c(3);
	for (int i = 0; i < 10; i++) {
		c.add(i);
		c.print();
	}
	return 0;
}
