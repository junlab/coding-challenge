/*
 * Implementation of the class that stores the cache objects (made on ubuntu)
 * Author: Valdir Dias Silva Junior
 * Email: valdir.d.siva.junior@gmail.com
 */

#include <iostream>
#include <string>

using namespace std;

#ifndef CACHE_H_
#define CACHE_H_

template <class T>
class Cache {
	public:
		Cache(int size);
		~Cache();
		bool complete;
		int front,size;
		struct RegCache {
			T cache;
			time_t time;
		};
		void add(T value);
		void print(bool ln=0);
	private:
		RegCache *circleQ;
};

#endif /* CACHE_H_ */
