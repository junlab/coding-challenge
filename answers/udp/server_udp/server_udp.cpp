/*
 * Server side implementation of UDP client-server model (made on ubuntu)
 * Author: Valdir Dias Silva Junior
 * Email: valdir.d.siva.junior@gmail.com
 */

#include <time.h>
#include <iostream>
#include <limits.h>
#include <cstdlib>
#include "server_udp.h"

char base16[16]= {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
using namespace std;

Server::Server(int port, int maxline) {
	this->maxline = maxline;
    // Creating socket file descriptor
    if ((this->sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("Server: socket creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&this->server_addr, 0, sizeof(this->server_addr));
    memset(&this->client_addr, 0, sizeof(this->client_addr));

    // Filling server information (IPv4)
    this->server_addr.sin_family    = AF_INET;
    this->server_addr.sin_addr.s_addr = INADDR_ANY;
    this->server_addr.sin_port = htons(port);

    // Bind the socket with the server address
    if (bind(this->sockfd, (const struct sockaddr *)&this->server_addr,
    		sizeof(this->server_addr)) < 0 ) {
        perror("Server: bind failed");
        exit(EXIT_FAILURE);
    }
    cout <<"Server start"<<endl;
}

void Server::timeout(int len) {
    struct timeval tv;
    tv.tv_sec = len;
    tv.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
}

Server::~Server() {
	close(this->sockfd);
}

void Server::upload() {
    // Generate random data
	int len_buff = this->data_generation_rate;
	char send_buff[len_buff];
	char reciv_buff[len_buff];
    for (int i = 0; i < len_buff; i++) {
		send_buff[i] = base16[rand() % 16];
	}
    send_buff[len_buff] = '\0';
    int current_second = -1;
    int count_buff;
	time(&init_com);
	time(&finish_com);
	int count_pack_send= 0;
	while(difftime(finish_com, init_com) < this->transfer_time) {
		if (current_second != difftime(finish_com, init_com)) {
			send((char*) send_buff);
			count_pack_send += strlen(send_buff);
			count_buff = len_buff;
			cout <<" "<< difftime(finish_com, init_com) << " - "
					<< difftime(finish_com, init_com)+1;
		} else if(count_buff > 0) {
			// New random buffer
			send_buff[rand()%(len_buff-1)] = base16[rand() % 16];
			count_buff--;
		}
		// Check timer
		current_second = difftime(finish_com, init_com);
		time(&finish_com);
	}
	cout<<" sec sent  "<<CHAR_BIT*count_pack_send<<" bits."<<endl;

}
void Server::send(char *str) {
	sendto(sockfd, (const char *)str, strlen(str),
			MSG_CONFIRM, (const struct sockaddr *) &this->client_addr,len);
	if(strstr(str, "xxxx") == NULL) {
		cout<<" sec sent "<<data_generation_rate<<"bytes at "<<CHAR_BIT*data_generation_rate
				<<"bits/sec"<<endl;
	} else {
		cout<<" Finished"<<endl;
	}
}

void Server::config_upload() {
    cout <<" Waiting for client"<<endl;
    struct sockaddr *actual_client_addr;

	this->len = sizeof(this->client_addr);
	char reciv_buff[this->maxline];
	int reciv_config_buff;

	int n;

	int count_recv = 0;
	while(count_recv<=1) {
		int n = recvfrom(this->sockfd, (char *)reciv_buff, this->maxline,
					MSG_WAITALL, (struct sockaddr *) &this->client_addr,&len);
		if (n) {
			count_recv++;
			n = 0;
		}

		if (count_recv == 1) {
			cout <<" New Client "<<endl;
			transfer_time = atoi(reciv_buff);
		} else if (count_recv == 2){
			data_generation_rate = atoi(reciv_buff);
		}
	}

	printf(" Transfer Time: %d\n", this->transfer_time);
	printf(" Data generation rate: %d\n", this->data_generation_rate);

	upload();
	send("xxxx");
	config_upload();
}

int main() {
    srand(time(0));
	Server *s = new Server(8069);
	s->config_upload();
}

