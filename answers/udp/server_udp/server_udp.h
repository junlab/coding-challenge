/*
 * Client.h
 *
 *  Created on: 12 de fev de 2020
 *      Author: ichiv
 */
#ifndef SERVER_UDP_H_
#define SERVER_UDP_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>

#include <arpa/inet.h>
#include <netinet/in.h>
class Server {
public:
	Server(int port, int maxline = 1024);
	~Server();
	void upload();
	void config_upload();
	void send(char *str);
	void timeout(int length);
	void recive();
	time_t init_com=0, finish_com=0;
	struct sockaddr_in server_addr, client_addr;
	int data_generation_rate = 0;
	int transfer_time = 0;
	unsigned int len=0;
    int maxline;
	int sockfd;
};

#endif /* UDP_H_ */
