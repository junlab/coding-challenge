/*
 * Client side implementation of UDP client-server model (made on ubuntu)
 * Author: Valdir Dias Silva Junior
 * Email: valdir.d.siva.junior@gmail.com
*/

#include <cstring>
#include <iostream>
#include <limits.h>
#include <time.h>
#include "client_udp.h"

using namespace std;

Client::Client(int port, int maxline) {
	this->maxline = maxline;
    // Creating socket
    if ((this->sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("Client: socket creation failed");
        exit(EXIT_FAILURE);
    }

    memset(&this->server_addr, 0, sizeof(this->server_addr));

    // Filling server information (IPv4)
    this->server_addr.sin_family    = AF_INET;
    this->server_addr.sin_addr.s_addr = INADDR_ANY;
    this->server_addr.sin_port = htons(port);

    cout <<"Start client"<<endl;
}

Client::~Client() {
	close(this->sockfd);
}

void Client::timeout(int len) {
    struct timeval tv;
    tv.tv_sec = len;
    tv.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
}

void Client::send(char *str) {
    this->len = sizeof(this->server_addr);
	sendto(sockfd, (const char *)str, strlen(str),
	        MSG_CONFIRM, (const struct sockaddr *) &this->server_addr,
	            len);
}

void Client::recive() {
    cout <<" Waiting for server"<<endl;
    this->len = sizeof(this->server_addr);
	char buffer[this->maxline];
	time(&init_com);
	int count_pack_recv = 0;
	while(strstr(buffer, "xx") == NULL) {
		int n = recvfrom(this->sockfd, (char *)buffer, this->maxline,
					MSG_WAITALL, ( struct sockaddr *) &this->server_addr,&len);
		buffer[n] = '\0';
		time(&finish_com);
		if (n && strstr(buffer, "xx") == NULL) {
			count_pack_recv += strlen(buffer);
			//cout<<" Recive from Server: "<<buffer<<endl;
			cout <<" "<< difftime(finish_com, init_com) << " - "
				<< difftime(finish_com, init_com)+1
				<<" sec recived "<< strlen(buffer)<<"bytes at "<<CHAR_BIT*strlen(buffer)
				<<"bits/sec"<<endl;
			n = 0;
		}
	}
	cout<<"Finished"<<endl;
	cout<<" 0 - "<<difftime(finish_com, init_com)<<" received "<<CHAR_BIT*count_pack_recv
			<<" bits with "<<data_generation_rate*transfer_time - count_pack_recv<< " packet loss."<<endl;
}

int main() {
	Client *c = new Client(8069, 1024);
    // Start and send transfer time
	c->send("10");
	c->transfer_time = 10;
	// Data generation rate
	c->send("10");
	c->data_generation_rate = 10;
	c->recive();
}
