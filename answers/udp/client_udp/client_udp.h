/*
 * Client side implementation of UDP client-server model (made on ubuntu)
 * Author: Valdir Dias Silva Junior
 * Email: valdir.d.siva.junior@gmail.com
 */

#ifndef CLIENT_UDP_H_
#define CLIENT_UDP_H_
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include <time.h>
class Client {
public:
    Client(int port, int maxline = 1024);
	~Client();
	void send(char *str);
	void recive();
	void timeout(int length);
	time_t init_com=0, finish_com=0;
	struct sockaddr_in server_addr;
	int data_generation_rate = 0;
	int transfer_time = 0;
    unsigned int len=0;
    int maxline;
    int sockfd;
};


#endif /* CLIENT_UDP_H_ */
